<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function showAboutPage(){
    	return view('end-user.about-us');
    }
}
