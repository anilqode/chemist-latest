<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use App;

class CheckoutStripeController extends Controller
{
	public function charge(Request $request)
	{
		try {

			\Stripe\Stripe::setApiKey('sk_live_dNEqDrKrXzSPBSTP2rwc2O9q00EARQfPDA');

			$amount =$request->stripeTotal * 100;

			$customer = Customer::create(array(
				'email' => $request->stripeEmail,
				'source' => $request->stripeToken
				));

			$charge = Charge::create(array(
				'customer' => $customer->id,
				'amount' => $amount,
				'currency' => 'USD'
				));

			 $message = 'Your Payment Have Heen Successfully Done';
			return redirect('/checkout')
                    ->with("success_message", $message);
		} catch (\Exception $ex) {
			return $ex->getMessage();
		}
	}
}