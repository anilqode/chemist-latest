<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactUsController extends Controller
{
    //

    public function contactUs(){
    	return view('end-user.contact-us');
    }

    public function gallery(){
    	$prod_cat=DB::table('product_categories')->get();
    	return view('end-user.gallery', compact('prod_cat'));
    
    }
}
