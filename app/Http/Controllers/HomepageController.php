<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\ProductCategory;
use App\Menu;
use App\Product;
use App\SubMenu;
use App\Slider;
use App\Repositories\HomePageRepository;

class HomepageController extends Controller
{

    private $homePageRepository;

    public function __construct(HomePageRepository $homePageRepository)
    {
        $this->homePageRepository = $homePageRepository;
    }

    public function index(){
    	$slider = Slider::all();
    	$main_menu = DB::table('menus')->orderBy('order')->get();
    	$sub_menu = DB::table('sub_menus')->get();
    	$prod_cat=DB::table('product_categories')->get();
    	return view('layout.index', compact('slider','prod_cat', 'main_menu', 'sub_menu'));
    }
    public function showSubmenuProducts($id, Request $request){

        $data = $this->homePageRepository->getSubmenuProducts($id, $request);

        list($sub_menu, $products, $priceFilter, $order, $sortArray, $priceFilterArray) = $data;

    	return view('sub-menu',compact('sub_menu', 'products', 'priceFilter', 'order', 'sortArray', 'priceFilterArray'));
    }
    public function showMainmenuProducts($id){
    	$main_menu = Menu::with(['subMenu' => function($q) {
            $q->orderBy('order', 'ASC');
        }])->findOrfail($id);

        $products = Product::OfMainMenu($id)->paginate(12);

    	return view('main-menu',compact('main_menu', 'products'));
    }
}