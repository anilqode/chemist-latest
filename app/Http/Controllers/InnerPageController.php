<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InnerPageController extends Controller
{
    public function ourGuarantee(){
    	return view('end-user.ourGuarantee');
    }

    public function privacyPolicy(){
    	return view('end-user.privacyPolicy');
    }
    public function termsOfUse(){
    	return view('end-user.termsOfUse');
    }
    public function termsandCondition(){
    	return view('end-user.termsandCondition');
    }
    public function deliveryInfo(){
        return view('end-user.deliveryInfo');
    }
    public function rewardPoints(){
        return view('end-user.rewardPoints');
    }
    public function deliveryInformation(){
        return view('end-user.deliveryInformation');
    }
}
