<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderProduct;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Response;
use Exception;
use App\Mail\DispatchEmail;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function dispatchOrder(Request $request, Order $order)
    {

        try {
            $order->dispatch_id = $request->dispatch_id;
            $order->dispatch_email = $request->dispatch_email;
            $order->save();

            $txt = '
                <html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <style type="text/css">
        body{
            font-family: "Roboto", sans-serif;
            padding: 0;
            margin: 0;
        }
        #wrapper{
            width: 70%;
            max-width: 600px;
            margin: 0 auto;
            box-shadow: 0px 3px 6px #000;
            border: 1px solid #ccc;
        }

        #header{

            width: 100%;
            height: 155px;
            padding: 20px 0 7px;
        }

        #header h4 {
            color: #000000;
            font-size: 30px;
            font-weight: 500;
            text-transform: uppercase;
        }


        #content h5 {
            color: #666666;
            font-size: 24px;
            font-weight: 700;
        }

        #content h6 {
            color: #04a7e0;
            font-size: 30px;
            font-weight: 700;
        }

        #content p {
            color: #666666;
            font-family: Roboto;
            font-size: 16px;
            font-weight: 400;
            margin-bottom: 70px;
        }

        #footeremail a{
            color:#fff;
        }

        #footer{
            background: #2170b5;
            height: 150px;
            width: 100%;
            padding: 20px 0 22px;
        }

        #footer p{
            color: #fff;
            font-size: 16px;
            font-weight: 400;
            line-height: 24px;
        }

        .text-center{
            text-align: center;
        }

        .top_30{
            margin-top: 30px;
        }

        #footer_social_icons li{
            display: inline-block;
            margin: 0px 3px;
            list-style: none;
        }
    </style>
</head>
<body>

<div id="wrapper" style="width:70%;margin:0 auto;"  class="text-center">
    <div id="header" style="background:#2170b5;">

        <img src="http://nutritionplanet.co.uk/images/homepage-images/nutritionplanet.jpg" style="
    outline: none;
    text-decoration: none;
    max-width: 100%;
    clear: both;
    display: block;
    margin: 0 auto;
    width: 282px;
    padding-bottom: 13px;
    padding-top: 10px;">
    </div><!-- #header -->
    <div id="content">
        <h2 style="text-align:center;color: #000000;font-family: Roboto;font-size: 24px;font-weight: 400;margin-top: 50px;margin-bottom: 0px;">Thank you again for your recent purchase at nutritionplanet.co.uk.</h2>
        <h2 class="attention" style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: 400;text-align: center;line-height: 1.3;word-wrap: normal;font-size: 14px;margin: 27px 0 10px;padding: 0;" align="center">Your order has been shipped. Tracking information for your order is available at <strong>'.$order->dispatch_id .'</strong><b></b></h2>
        <h2 class="attention" style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: 400;text-align: center;line-height: 1.3;word-wrap: normal;font-size: 14px;margin: 27px 0 10px;padding: 0;" align="center">We hope you enjoyed shopping at nutritionplanet.co.uk. We look forward to your continued business.<b></b></h2>

    </div><!-- #content -->
    <div id="footer" style="
    background: #2170b5;
    height: 150px;
    width: 100%;
    padding: 20px 0 22px;
    text-align: center;
    color: #fff;
    margin-top: 32px;">

        <img src="http://nutritionplanet.co.uk/images/homepage-images/nutritionplanet.jpg" alt="" width="" style="    width: 40%;
    margin: 0 auto;"/>

         <p>Devonshire House 582 Honeypot Lane Stanmore Middlesex HA7 1JS <br />




            <span id="footeremail" style="color:#fff;"><a style="color:#fff;">enquiry@nutritionplanet.co.uk</a></span> | 020 8732 5465</p>

    </div><!-- #footer -->


</div><!-- #wrapper -->
</body>
</html>';
$to = $order->dispatch_email;
        $subject = "Your nutritionplanet.co.uk shipping confirmation.";
        // $txt = $txt1 . $customInfo . $txt3;
        $headers = "From:nutritionplanet.co.uk\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
        $result=   mail($to,$subject,$txt,$headers);

            // Mail::to($order->dispatch_email)->send(new DispatchEmail($order));
        } catch(Exception $e) {
            return redirect()->back()->with('error', "Error in saving dispatch info.");
        }

        return redirect()->back()->with('success_message', 'Dispatch info saved successfully.');


    }

    public function reOrder(Request $request) {

        $order = Order::with(['orderProduct', 'orderProduct.product'])->where('id',$request->id)->first();
        foreach ($order->orderProduct as $orderProduct) {
            $product = $orderProduct->product;
            Cart::add($product->id, $product->name, $orderProduct->quantity, $product->price, ['image'=>$product->featured_image1, 'availableQuantity'=> (int)$product->available_quantity])
            ->associate('App\Product');
        }

        return redirect()->action('CartController@index')->with('success_message', 'Reordered items are added to your cart!');

    }

    public function orderhistory()
    {
        $user = Auth::user();

        if($user->hasRole('super-admin')) {
            $orders = Order::with(['delivery','orderProduct', 'orderProduct.product'])->orderBy('created_at', 'DESC')->get();
            return view('admin.order', compact('orders'));
        } else {
            $orders = Order::with(['delivery','orderProduct', 'orderProduct.product'])->where('user_id', $user->id)->orderBy('created_at', 'DESC')->get();
            return view('users.order-history', compact('orders'));
        } 
    }



    public function export()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=orders.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $user = Auth::user();

        if($user->hasRole('super-admin')) {
            $params = Order::with(['delivery','orderProduct', 'orderProduct.product'])->orderBy('created_at', 'DESC')->get();   
          
        } else {
            $params = Order::with(['delivery','orderProduct', 'orderProduct.product'])->where('user_id', $user->id)->orderBy('created_at', 'DESC')->get();
          
        }
        
        $columns = array('S.N','Order Id', 'Date of Order', 'Product details', 'Tax', 'Amount', 'Customer Name', 'Phone Number', 'Email', 'Delivery Address');


        $callback = function() use ($params, $columns)
        {
            $counter = 0;
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($params as $orders) {
                $delivery_address = 'City:- '.$orders->delivery_city.', '.'Address:- '.$orders->delivery_address.', '.'Postal Code:- '.$orders->delivery_postalcode;

                $products = $orders->orderProduct;
                $orderedProductDetails = "";
                $orderedProduct = array($orderedProductDetails);
                    
                foreach($products as $product) 
                    {
                        $orderedProductDetails = $product->product['name'];
                          $quantityPotency = '( Quantity: '.$product->quantity . ' ) ( Potency : ' . $product->product['potency'] . ' )';
                           array_push($orderedProduct, $orderedProductDetails, $quantityPotency); 
                    }
                   
                $proString = implode("=>", $orderedProduct);

                fputcsv(
                    $file, 
                    array(
                        ++$counter,
                        $orders->id,
                        $orders->created_at,
                        $proString, 
                        $orders->billing_tax, 
                        $orders->billing_total, 
                        $orders->delivery_name, 
                        $orders->delivery_phone, 
                        $orders->delivery_email, 
                        $delivery_address
                    )
                );
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }
}
