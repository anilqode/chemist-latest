<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use App\Product;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Repositories\ProductRepository;

class ProductController extends Controller
{

  private $productRepository;

  public function __construct(ProductRepository $productRepository)
  {
      $this->productRepository = $productRepository;
  }

 public function index(){

  $product_cat=ProductCategory::pluck('name','id');
 //return $product_cat;

  $stock[0]='In Stock';
  $stock[1]='Out Of Stock';
   //	return $product_cat;

  return view('admin.product.create_product', compact('product_cat','stock'));
  
}

public function displayProdCatList(){
  $prod_cat=DB::table('product_categories')->get();
  return view('end-user.product-category-list', compact('prod_cat'));
}


public function store(Request $request){

  $this->productRepository->save($request);

  return redirect('/product')->with('success','Product Created Successfully');

}

public function show(){
  $products = Product::all();
  //return $products;
  return view('admin.product.show', compact('products'));
}
public function delete($id){
  $products = Product::findorFail($id);
  $products->delete();
  return redirect('product/show')->with('success', 'Product was deleted Successfully!!!');
}


public function edit($id){
  $product_cat=ProductCategory::pluck('name','id');
  $product_edit = Product::findorFail($id);
  $selected_categories = $product_edit->productMultipleCategory->pluck('product_category_id');

  $stock[0]='In Stock';
  $stock[1]='Out Of Stock';
  return view('admin.product.edit', compact('product_edit', 'product_cat', 'stock', 'selected_categories'));
}

public function update(Request $request, $id){
  $this->productRepository->update($request, $id);
  return redirect('/product/show')->with('success', 'Product edited Successfully');
}
public function visualize( $id){
  $product_show = Product::findorFail($id);
  return view('display', compact('product_show'));
}
}