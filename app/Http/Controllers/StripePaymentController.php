<?php

namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use Session;
use Stripe;
use App\Order;
use App\Repositories\CheckoutRepository;
use App\Coupon;


class StripePaymentController extends Controller
{
    private $checkoutRepository;

    public function __construct(CheckoutRepository $checkoutRepository)
    {
        $this->checkoutRepository = $checkoutRepository;
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {   
        // dd('Hello');
        return view('stripe.stripe');
    }
  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        Stripe\Charge::create ([
                "amount" => session()->get('amount') * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from Nutrition Planet." 
        ]);

        Session::flash('success', 'Payment successful!');
        $coupon = null;
        if ($request->session()->exists('coupon')) {
            $couponData = $request->session()->get('coupon');
            $coupon = $couponData['id'];
        }
        // dd($coupon);
        $orderDetails = $this->checkoutRepository->addToOrdersTables(session()->get('data'), $request->session()->get('delivery'), $coupon);
        if(!empty($coupon)) {
            $coupon = Coupon::find($coupon);
            $coupon->coupon_applied_times = (int)$coupon->coupon_applied_times+1;
            $coupon->save();
        }
        $request->session()->forget('data');
        $request->session()->forget('cart');
        $request->session()->forget('delivery');
        $request->session()->forget('coupon');
        $request->session()->forget('amount');

        $emailCtrl = new EmailController();

        $emailCtrl->SendEmailToAdmin($orderDetails);
        $emailCtrl->sendEmailToCustomer($orderDetails);
        if(auth()->user()) {
            return redirect('order-history');//->with('order_id',$order_id);
        }
        else {
                $message = 'Thank You! Your order has been placed successfully. We will contact you soon.';

                return redirect(route('payment-confirmation'))
                ->with("success_message", $message);
        }
        return view('payment-confirmation');
        // return back();
    }

    public function checkout(Request $request){
        
        session()->put('data', $request->all());
        session()->put('amount', $request->amount);

        return redirect('/stripe');
    }
}