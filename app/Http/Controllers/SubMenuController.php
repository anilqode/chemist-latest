<?php

namespace App\Http\Controllers;

use App\Menu;
use App\SubMenu;
use Illuminate\Http\Request;

class SubMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $main_Menu = Menu::all();
        $sub_Menu = SubMenu::orderBy('mainmenuid', 'ASC')->orderBy('order', 'ASC')->get();
        return view('subMenu.index', compact('main_Menu', 'sub_Menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $main_Menu = Menu::all();
        return view('subMenu.create', compact('main_Menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'subMenuName' => 'required|max:40',
            'mainmenuid' => 'required',
            ));
        $submenu = new SubMenu();
        $submenu->subMenuName = $request->input('subMenuName');
        $submenu->mainmenuid = $request->input('mainmenuid');
        $submenu->order = $request->input('order');
        $submenu->save();
        return redirect('/subMenu')->with('Success', 'Submenu is added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubMenu  $subMenu
     * @return \Illuminate\Http\Response
     */
    public function show(SubMenu $subMenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubMenu  $subMenu
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $main_Menu = Menu::pluck('menuName','id'); 
        $submenu = SubMenu::findorFail($id);
        return view('subMenu.edit', compact('main_Menu','submenu', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubMenu  $subMenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'subMenuName' => 'required|max:40',
            'mainmenuid' => 'required',
            ));
        $submenu = SubMenu::findorFail($id);
        $submenu->subMenuName = $request->input('subMenuName');
        $submenu->mainmenuid = $request->input('mainmenuid');
        $submenu->order = $request->input('order');
        $submenu->save();
        return redirect('/subMenu')->with('Success', 'Submenu is added successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubMenu  $subMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $submenu = SubMenu::findorFail($id);
        $submenu->delete();
        return redirect('/subMenu')->with('Success', 'Submenu is deleted successfully.');
    }
}
