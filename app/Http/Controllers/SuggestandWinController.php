<?php

namespace App\Http\Controllers;

use App\SuggestandWin;
use Illuminate\Http\Request;
use App\Repositories\SuggestAndWinRepository;
use App\Http\Requests\CreatesuggestandwinRequest;

class SuggestandWinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    private $suggestandwinRepository;

    public function __construct(SuggestAndWinRepository $suggestandwinRepository){
        $this->suggestandwinRepository = $suggestandwinRepository;
    }

    public function index()
    {
        return view('end-user.suggestandWin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatesuggestandwinRequest $request)
    {
        $result = $this->suggestandwinRepository->create($request);
        return redirect()->back()->with('Success', 'Your form has been submitted.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SuggestandWin  $suggestandWin
     * @return \Illuminate\Http\Response
     */
    public function show(SuggestandWin $suggestandWin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SuggestandWin  $suggestandWin
     * @return \Illuminate\Http\Response
     */
    public function edit(SuggestandWin $suggestandWin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SuggestandWin  $suggestandWin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SuggestandWin $suggestandWin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SuggestandWin  $suggestandWin
     * @return \Illuminate\Http\Response
     */
    public function destroy(SuggestandWin $suggestandWin)
    {
        //
    }
}
