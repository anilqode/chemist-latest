<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value_money'=>'required',
            'quality' => 'required',
            'performance' => 'required',
            'overall' => 'required',
            'user_id' => 'integer|nullable',
            'name' => 'max:30|nullable',
            'email' => 'nullable|max:255',
            'recommend' => 'nullable',
            'reviewBox' => 'nullable|max:500',
            'product_id' => 'required'
        ];
    }
}