<?php

namespace App\Mail;

use App\Order;
use App\OrderProduct;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $orderDetails;
    public $order_products;
    public $order_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($orderDetails)
    {
        $this->orderDetails = $orderDetails;
        $order_id=$orderDetails->id;
        $this->order_id=$order_id;
        $order_products=Order::find($order_id)->orderProduct;
        $this->order_products=$order_products;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

       // dd($this->orderDetails);
        return $this->view('emails.AdminEmail')->with([
            'orderDetails' => $this->orderDetails,
            'order_products' => $this->order_products
        ]);;
    }
}
