<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
	protected $fillable=[ 'order_id',
	'product_id',
	'quantity'];

	/**
	 * Ordered Products belonged tothe order
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function order()
	{
	    return $this->belongsTo(Order::class, 'order_id');
	}

	/**
	 * Ordered Products belonged tothe order
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function product()
	{
	    return $this->belongsTo(Product::class, 'product_id');
	}
}
