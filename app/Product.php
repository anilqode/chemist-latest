<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable=['name', 'description', 'product_category_id', 'featured_image1', 'featured_image2', 'featured_image3', 'price', 'potency', 'size', 'short_intro', 'discount', 'in_out_stock', 'available_quantity', 'sku', 'directions', 'ingredients', 'meta_title', 'meta_description', 'h1', 'h2', 'h3', 'warnings'];

    public function scopeMightAlsoLike($query)
    {
        return $query->inRandomOrder()->take(4);
    }

    public function presentPrice()
    {
        return '£' . number_format($this->price, 2);
    }

    /**
     * Get the categories names of the product.
     */
    public function getCategoriesNameAttribute()
    {
        $multipleCategories = $this->productMultipleCategory()->get();
        $categoriesNames = [];
        foreach ($multipleCategories as $multipleCatetory) {
            if(!empty($multipleCatetory->productCategory)) {
                $categoriesNames[] = $multipleCatetory->productCategory->name;
            }
        }
        if(!empty($categoriesNames)) {
            return implode(', ', $categoriesNames);
        }
        return null;
    }

    public function scopeOfCategory($query, $categoryId)
    {
        return $query->whereHas('productMultipleCategory', function ($q) use ($categoryId) {
            $q->where('product_category_id', '=', $categoryId);
        });
    }

    public function scopeOfMainMenu($query, $menuId)
    {
        return $query->whereHas('productMultipleCategory', function ($q) use ($menuId) {
            $q->whereHas('productCategory', function($catQuery) use ($menuId) {
                $catQuery->where('selectmainmenu', '=', $menuId);
            });
        });
    }

    public function scopeOfSubMenu($query, $subMenuId)
    {
        return $query->whereHas('productMultipleCategory', function ($q) use ($subMenuId) {
            $q->whereHas('productCategory', function($catQuery) use ($subMenuId) {
                $catQuery->where('selectsubmenu', '=', $subMenuId);
            });
        });
    }


    /**
     * Ordered Products associated with the product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderProduct()
    {
        return $this->hasMany(OrderProduct::class);
    }

    /**
     * Product can belongs to multiple categories
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productMultipleCategory()
    {
        return $this->hasMany(ProductMultipleCategory::class);
    }

}
