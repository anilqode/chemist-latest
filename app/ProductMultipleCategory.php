<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMultipleCategory extends Model
{
	protected $fillable=['product_id', 'product_category_id'];

    /**
	 * Products belonged to the multiple category
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function product()
	{
	    return $this->belongsTo(Product::class, 'product_id');
	}

	/**
	 * Products belonged to the multiple category
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function productCategory()
	{
	    return $this->belongsTo(ProductCategory::class, 'product_category_id');
	}
}
