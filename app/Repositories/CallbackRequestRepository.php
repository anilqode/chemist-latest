<?php

namespace App\Repositories;

use App\RequestCallback;
use Exception;

/*
**Callback Request Repository Class
 */

class CallbackRequestRepository{

	/**
	 * Create a new callback Request
	 * @param type $request 
	 * @return type
	 */
	
	public function create($request){
		$input = $request->all();
		$result = RequestCallback::create($input);
		return $result;
	}
}