<?php  

namespace App\Repositories;

use App\Order;
use App\OrderProduct;
use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;

use Exception;
/**
 * Checkout Repository class
 */
class CheckoutRepository
{
	public function addToOrdersTables($checkoutData, $delivery, $coupon)
	{
		$request = (object)$checkoutData;
	    // Insert into orders table
	    $order = Order::create([
	        'user_id' => auth()->user() ? auth()->user()->id : null,
	        'delivery_id' => $delivery['id'],
	        'coupon_id' => $coupon,
	        'delivery_price' => $delivery['price'],
	        'billing_email' => $request->email,
	        'billing_name' => $request->name,
	        'billing_address' => $request->address,
	        'billing_city' => $request->city,
	        'billing_province' => $request->province,
	        'billing_postalcode' => $request->postalcode,
	        'billing_phone' => $request->phone,

	        'delivery_email' => $request->delivery_email,
	        'delivery_name' => $request->delivery_name,
	        'delivery_address' => $request->delivery_address,
	        'delivery_city' => $request->delivery_city,
	        'delivery_province' => $request->delivery_province,
	        'delivery_postalcode' => $request->delivery_postalcode,
	        'delivery_phone' => $request->delivery_phone,

	        'billing_discount' => getNumbers()->get('discount'),
	        'billing_discount_code' => getNumbers()->get('code'),
	        'billing_subtotal' => getNumbers()->get('newSubtotal'),
	        'billing_tax' => getNumbers()->get('newTax'),
	        'billing_total' => getNumbers()->get('newTotal')

	    ]);

	    // Insert into order_product table
	    foreach (Cart::content() as $item) {
	    	$this->reduceProductQuantity($item->model->id, $item->qty);
	        OrderProduct::create([
	            'order_id' => $order->id,
	            'product_id' => $item->model->id,
	            'quantity' => $item->qty,
	        ]);
	    }

	    return $order;
	}

	/**
	 * Reduce the product Quantity after purchase.
	 */
	public function reduceProductQuantity($productId, $quantity)
	{
		$product = Product::find($productId);
		$product->available_quantity = (int)$product->available_quantity - (int)$quantity;
		$product->save();
	}

}