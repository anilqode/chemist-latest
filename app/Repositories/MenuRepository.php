<?php

namespace App\Repositories;

use App\Menu;

/**
 * 
 */
class MenuRepository
{
	
	public function __construct()
	{
		
	}

	public function getMenus()
	{
		$main_menus = Menu::all()->sortBy('order');
		$menus = [];
		$menuContents = [];
		foreach ($main_menus as $menu) {
			$tempMenu = $menu->toArray();
			$menuContents = [];
			$menuArray = [];
			foreach ($menu->submenu as $subMenu) {
				$menuContents[] = [
					'type' => 'sub-menu',
					'id' => $subMenu->id,
					'name' => $subMenu->subMenuName,
				];
				if(count($menuContents) == 15) {
					$menuArray[] = $menuContents;
					$menuContents = [];
				}
				foreach ($subMenu->productCategory as $category) {
					$menuContents[] = [
					'type' => 'category',
					'id' => $category->id,
					'name' => $category->name];
					if(count($menuContents) == 15) {
						$menuArray[] = $menuContents;
						$menuContents = [];
					}
				}
			}
			if(!empty($menuContents)) {
				$menuArray[] = $menuContents;
			}
			$tempMenu['contents'] = $menuArray;
			$menus[] = $tempMenu;
		}
		return $menus;
	}
}