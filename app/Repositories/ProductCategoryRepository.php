<?php

namespace App\Repositories;

use App\Repositories\HomePageRepository;
use App\ProductCategory;
use App\Product;
/**
 * 
 */
class ProductCategoryRepository
{
	public function getProductsByCategoryId($cat_id, $request)
	{
        $cat_name=ProductCategory::where('id',$cat_id)->first()->name;

        $order = $request->sort;

        $sortUrl = '';

        $priceFilter = '';
        $count = !empty($request->count)?$request->count:12;
        if(isset($request->price)) {
            $priceFilter = $request->price;
            if($request->price === '5') {
                $products = Product::ofCategory($cat_id)->where('price', '<', (int)$request->price);
            }
            else if($request->price === '50') {
                $products = Product::ofCategory($cat_id)->where('price', '>', $request->price);
            } else {
                $priceRange = explode('-', $request->price);
                $products = Product::ofCategory($cat_id)
                    ->where('price', '>', $priceRange[0])
                    ->where('price', '<=', $priceRange[1]);
            }
            $sortUrl = '?price='.$priceFilter.'&';
        } else {
            $products = Product::ofCategory($cat_id);
            $sortUrl = '?';
        }

        if(!empty($order)) {
            $sortOrder = explode('_', $order);
            $products = $products->orderBy($sortOrder[0], $sortOrder[1]);
        }

        $products = $products->paginate($count);

        $homaPageRepository = new HomePageRepository();
        $sortArray = $homaPageRepository->getSortArray($sortUrl);

        $priceFilterArray = $homaPageRepository->getPriceFilterArray($order);

        return [
        	$cat_name,$products,$priceFilter,$order,$sortArray, $priceFilterArray
        ];
	}
}