<?php  

namespace App\Repositories;

use App\Slider;
use App\Services\ImageService;
use Exception;
/**
 * Slider Repository class
 */
class SliderRepository
{
	/**
	 * @var Imageservice
	 */
	protected $imageService;
	
	/**
	 * Slider constructor
	 * @param ImageService $imageService 
	 * @return type
	 */
	public function __construct(ImageService $imageService)
	{
		$this->imageService = $imageService;
	}

	/**
	 * Create a new slider image
	 * @param type $request 
	 * @return type
	 */
	public function create($request) {

		$input = $request->all();
		$input['photo'] = $this->imageService->saveImage($request->file('photo'), 'images/homepage-images/');
		$result = Slider::create($input);

		return $result;
	}

	/**
	 * Update the slider
	 * @param type $slider 
	 * @param type $request 
	 * @return type
	 */
	public function update($slider, $request) {

		$input = $request->all();

		if($request->hasFile('photo')){
			$input['photo'] = $this->imageService->saveImage($request->file('photo'), 'images/homepage-images/');
		} else {
			unset($input['photo']);
		}
		return $slider->update($input);
	}
}