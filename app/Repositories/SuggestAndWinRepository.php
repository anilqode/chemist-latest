<?php

namespace App\Repositories;

use App\SuggestandWin;
use Exception;
/**
 * SuggestAndWinRepository class
 */
class SuggestAndWinRepository{

	public function create($request){
		$input = $request->all();
		$result = SuggestandWin::create($input);
		return $result;
	}
}