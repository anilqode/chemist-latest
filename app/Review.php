<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
    	'value_money', 'quality', 'performance', 'overall', 'user_id', 'name', 'email', 'recommend', 'reviewBox', 'product_id'
    ];
}
