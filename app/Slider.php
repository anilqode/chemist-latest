<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
    	'title', 'photo', 'alt','description'
    ];

    protected $visible = ['image_url'];

    protected $appends = ['image_url'];

   /**
     * Get the full path for the post image
     *
     * @return null|string
     */
    public function getImageUrlAttribute()
    {
        return $this->photo ? url('storage/images/homepage-images/' . $this->photo) : null;
    }
}
