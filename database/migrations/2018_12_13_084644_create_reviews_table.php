<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('value_money');
            $table->integer('quality');
            $table->integer('performance');
            $table->float('overall');
            $table->integer('user_id')->nullable();
            $table->string('name')->nullable();
            $table->text('email')->nullable();
            $table->boolean('recommend')->nullable();
            $table->text('reviewBox')->nullable();
            $table->timestamps();
            // $table->foreign('user_id')
            //         ->references('id')
            //         ->on('users')
            //         ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
