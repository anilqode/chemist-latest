<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliveryDetailsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->text('delivery_email')->nullable()->after('billing_phone');
            $table->text('delivery_name')->nullable()->after('delivery_email');
            $table->text('delivery_address')->nullable()->after('delivery_name');
            $table->text('delivery_city')->nullable()->after('delivery_address');
            $table->text('delivery_province')->nullable()->after('delivery_city');
            $table->text('delivery_postalcode')->nullable()->after('delivery_province');
            $table->text('delivery_phone')->nullable()->after('delivery_postalcode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('delivery_email');
            $table->dropColumn('delivery_name');
            $table->dropColumn('delivery_address');
            $table->dropColumn('delivery_city');
            $table->dropColumn('delivery_province');
            $table->dropColumn('delivery_postalcode');
            $table->dropColumn('delivery_phone');
        });
    }
}
