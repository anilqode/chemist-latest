<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coupon_type');
            $table->string('coupon_code')->unique();
            $table->string('discount_type');
            $table->string('discount_value');
            $table->string('minimum_amount')->nullable();
            $table->string('coupon_use_type');
            $table->integer('coupon_applicable_times')->nullable();
            $table->integer('coupon_applied_times')->nullable();
            $table->boolean('visible');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
