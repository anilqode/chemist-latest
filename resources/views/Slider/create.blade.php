@extends('layouts.admin')

@section('content')
    <div class="container-fluid slider-create">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <form method="post" action="{{url('/slider/create')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <input type="hidden" value="{{csrf_token()}}" name="_imgtitle" />
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" name="title"/>
                </div>
                <div class="form-group">
                    <input type="hidden" value="{{csrf_token()}}" name="_imgpath"/>
                    <label for="photo">Photo:</label>
                    <input type="file" class="form-control" name="photo" accept="image/png, image/jpeg, image/jpg"/>
                </div>
                <div class="form-group">
                    <input type="hidden" value="{{csrf_token()}}" name="_alt"/>
                    <label for="alt">Alt:</label>
                    <input type="text" class="form-control" name="alt"/>
                </div>
                <div class="form-group">
                    <input type="hidden" value="{{csrf_token()}}" name="_description"/>
                    <label for="description">Description:</label>
                    <input type="text" class="form-control" name="description"/>
                </div>
                <div class="form-group">
                    <button type="submit"  class="pull-right btn btn-primary">Add Slide</button>
                </div>
            </form>
        </div>
    </div>

@endsection