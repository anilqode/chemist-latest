@extends('layouts.admin')

@section('content')
<div class="container-fluid">
	@if($errors->any())
	<div class="alert alert-danger">
		<ul>
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
		</ul><br/>
	</div>
	@endif
	<div class="row">
		<form method="POST" action="/slider/edit/{{ $slider->id }}" enctype="multipart/form-data">
		@csrf
			<div class="form-group">
				<label for="title">Title:</label>
				<input type="text" class="form-control" name="title" value='{{ $slider->title }}' />
			</div>
			<div class="form-group">
	            <label for="photo">Photo:</label>
	            <input type="file" class="form-control" name="photo" accept="image/png, image/jpeg, image/jpg" />
	        </div>
	        <img src="{{$slider->image_url}}" width="150px" alt="image">
	        <div class="form-group">
	            <label for="alt">Alt:</label>
	            <input type="text" class="form-control" name="alt" value='{{ $slider->alt }}'/>
	        </div>
	        <div class="form-group">
	            <label for="description">Description:</label>
	            <input type="text" class="form-control" name="description" value='{{ $slider->description}}'/>
	        </div>
	        <div class="form-group">
	            <button type="submit"  class="pull-right btn btn-primary">Update Slide</button>
	        </div>
		</form>
	</div>
</div>
@endsection