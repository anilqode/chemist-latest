@section('title', 'Create Product')
@extends('layouts.admin')
@push('mystyles')
	<link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}">
	<link href="{{ url('/css/clientdashboard.css') }}" rel="stylesheet">

	<style type="text/css">
		.roles-list-title{
			top: -6px !important;
			margin-bottom: 8px;
			margin-left:12px;
		}
		.inventrary_list {
			/*box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);*/
			border-radius: 10px;
			background-color: #ffffff !important;
			min-height: 556px;
		}
		.ptpl{
			padding-top: 15px;
			padding-left: 29px;
		}
		.create-form-group select{
			width: 100%;
			margin: 14px 0 0 0;
		}
		.card_img_box {
			padding: 40px 20px 36px !important;
		}
		.dealer_setting_img img, .dealer_logo_url {
			max-width: 70px !important;
			/* width: 100%; */
		}
		.card_img_box h6 {
			margin-top: 5px;
			color: #999999;
			font-weight: 400 !important;
			font-size: 18px;
			padding-top: 34px !important;
		}
		select {
			width: 100%;
			height: 50px !important;
			border-radius: 10px !important;
			border: 1px solid #cccccc;
			background: url(/img/down_arrow.png) 98.5% no-repeat;
			background-color: #ffffff;
			-webkit-appearance: none;
			float: right;
			font-size: 18px !important;
			color: #999 !important;
		}
		.main-header.navbar.navbar-expand.bg-white.navbar-light.border-bottom {
    margin: 0px;
}
	</style>
@endpush
@section('content')

	<div class="container-fluid container-wrapper mt-19">
		<section class="p_15 mb-63 dashboard_inventary">
			<div class="row">
						<div class="col-sm-12">
							<div class="roles-list-title">
								<h4>Create Slider</h4>
							</div>
						</div>
						
						<div class="clearfix"></div>
					</div>

					<a class="btn btn-primary" href="{{ url('/slider/create')}}" style="margin-bottom:30px;">Add new Slide</a>
					<div class="row" style="margin-bottom:30px;">
						@foreach($sliders as $slide)
							<div class="col-sm-4">
								<img src="{{$slide->image_url}}" alt=" {{ $slide->alt }}" width="100%"/>
								<a href="/slider/edit/{{$slide->id}}" class="btn btn-block btn-info">Edit</a>
								<a href="/slider/delete/{{$slide->id}}" onclick="deleteAction(event)" class="btn btn-block btn-danger">Delete</a>
							</div>
						@endforeach
					</div>
		</section>
	</div><!-- .container-fluid -->
@endsection
@push('myScripts')
	<script type="text/javascript" src="{{ url('/js/jquery.min.js') }}"></script>

@endpush
