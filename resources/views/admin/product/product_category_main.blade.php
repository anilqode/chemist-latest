@section('title', 'Create Product Category')
@extends('layouts.admin')
@push('mystyles')
    <link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}">
    <link href="{{ url('/css/clientdashboard.css') }}" rel="stylesheet">

    <style type="text/css">
        .roles-list-title{
            top: -6px !important;
            margin-bottom: 8px;
            margin-left:12px;
        }
        .inventrary_list {
            /*box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);*/
            border-radius: 10px;
            background-color: #ffffff !important;
                min-height: 556px;
        }
        .ptpl{
                padding-top: 15px;
    padding-left: 29px;
        }
        .create-form-group select{
            width: 100%;
            margin: 14px 0 0 0;
        }
        .card_img_box {
            padding: 40px 20px 36px !important;
        }
        .dealer_setting_img img, .dealer_logo_url {
            max-width: 70px !important;
            /* width: 100%; */
        }
        .card_img_box h6 {
            margin-top: 5px;
            color: #999999;
            font-weight: 400 !important;
            font-size: 18px;
            padding-top: 34px !important;
        }
        select {
            width: 100%;
            height: 50px !important;
            border-radius: 10px !important;
            border: 1px solid #cccccc;
            background: url(/img/down_arrow.png) 98.5% no-repeat;
            background-color: #ffffff;
            -webkit-appearance: none;
            float: right;
            font-size: 18px !important;
            color: #999 !important;
        }
    </style>
@endpush
@section('content')

  <div class="container-fluid container-wrapper mt-19">
        <section class="p_15 mb-63 dashboard_inventary">
            <div class="row">
                <div class="col-sm-3">
                    @include('layouts.sidebar')
                </div>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-11">
                            <div class="roles-list-title">
                                <h4>Create Product Category</h4>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="row ptpl">
                        <div class="col-sm-12 inventrary_list">
                            <div class="customer_vehicle_form">
                                {!! Form::open(['url' => "/product-category/",'enctype'=>'multipart/form-data','file'=>true,'method' => 'POST', 'class' => 'form-horizontal login-form']) !!}
                                {{ csrf_field() }}
                                    <div class="row top_50">
                                        <div class="col-sm-2 picture-container dealer_setting_img">
                                            <div class="card_img_box picture top_10">
                                            
                                                <img src="{{ url('images/icons/card_upload.svg') }}" class="dealer_logo_url">
                                                <input type="file" name="featured_image" class="card_img_file">
                                                <h6>Upload Photo</h6>
                                            </div>
                                         
                                        </div>
                                        <div class="col-sm-10 create-form-group pl-30">
                                            <div class="row mt-6n">
                                               <div class="col-sm-6">
                                                    {!! Form::text('name','', ['class' => 'form-control','placeholder' => 'Category Name']) !!}
                                                </div>
                                                <div class="col-sm-6 pl-25">
                                                    {!! Form::number('order','', ['class' => 'form-control order','placeholder' => 'Order']) !!}
                                                </div>

                                            </div>

                                           
                                            <!-- <div class="row mt-24">
                                                <div class="col-sm-6">
                                                   {!! Form::text('price','', ['class' => 'form-control','placeholder' => 'Price']) !!}
                                                </div>
                                                <div class="col-sm-6 pl-25">
                                                    {!! Form::text('discount','', ['class' => 'form-control','placeholder' => 'Discount']) !!}
                                                </div>
                                            </div> -->

                                            <div class="row mt-24">
                                                <div class="col-sm-6">

                                                        {!! Form::select('mainmenu', $mainmenu, null, ['class' => 'form-control','id'=>'mainmenu','placeholder' => 'Select main menu']); !!}
                                               
                                                </div>
                                                <div class="col-sm-6 pl-25">

                                                    {!! Form::select('subMenuName', $submenu, null, ['class' => 'form-control','id'=>'subMenu','placeholder' => 'Select Sub menu']); !!}


                                                    {{--<input type="hidden" class="form-control" name="mainmenuidvalue" id="mainmenuidvalue"/>--}}
                                                    {{--<select class="form-control" name="selectsubmenu" id="submenu">--}}
                                                       {{--<option>Select Sub Category</option>--}}
                                                       {{--@foreach($submenu as $sub)--}}
                                                                {{--<option>{{$sub->subMenuName}}</option>--}}
                                                       {{--@endforeach--}}
                                                   {{--</select>--}}
                                                </div>
                                            </div>

                                             <div class="row mt-24" style="margin-top:20px;">
                                                <div class="col-sm-12 mt-24">
                                                   {!! Form::textarea('description','', ['class' => 'form-control text-area','placeholder' => 'Description']) !!}
                                                </div>
                                               
                                            </div>
                                            <div class="row mt-24">
                                                <div class="col-sm-6">
                                                    <a href="#" class="btn btn-danger btn-lg btn-block">Cancel</a>
                                                </div>
                                                <div class="col-sm-6 pl-25">
                                                    {{ Form::submit('Save', ['class' => 'btn btn-primary btn-lg btn-block']) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                {!! Form::close() !!}

                            </div><!-- .customer_vehicle_form -->
                        </div>
                    </div>
                </div>

            </div><!-- .row -->
        </section>
    </div><!-- .container-fluid -->

@endsection
@push('myScripts')
    <script type="text/javascript" src="{{ url('/js/jquery.min.js') }}"></script>

    <script type="text/javascript">

        $.ajaxSetup({
            headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
        });

        $("#mainmenu").on("change", function () {
            var modelid = $(this).val();

            //Here you can use ajax to call php function
            $.ajax({
                url: '/find-sub-menu/',
                type: 'GET',
                dataType: "json",
                data: {
                    menu_id: menuId
                },
                success: function (ret_data) {


                    $('select[name="submenu"]').empty();

                    $('select[name="submenu"]').append('<option value="0">Select Sub Menu</option>');
                    $.each(ret_data, function (key, value) {


                        $('select[name="series"]').append('<option value="' + value['series'] + '">' + value['series'] + '</option>');
                    });


                }
            });
        });


    </script>
   
@endpush