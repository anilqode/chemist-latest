@extends('layouts.admin')

@section('content')
<div class="container-fluid">

	<div class="row">
		<div class="col-sm-3 admin-sidebar">
			@include('layouts.sidebar')
		</div>
		<div class="col-sm-9 admin_playzone">

			<table id="example" class="table table-hover product-list-display display_product" style="width:100%">
				<thead>
					<tr>
						<th>Id</th>
						<th>Product Image</th>
						<th>Product Name</th>
						<th>Product Category</th>
						<th>Price</th>
						<th>Available Quantity</th>
						<th>Stock Status</th>
						<th>Description</th>
						<th>Action</th>
					</tr>
				</thead>

				<tbody>
					@foreach($products as $product)
					<tr>

						<td>{{ $product->id }}</td>
						<td><img src="/{{ $product->featured_image1 }}" alt="{{ $product->name }}" width="100"/></td>
						<td>{{ $product->name }}</td>
						<td>{{ $product->categoriesName }}</td>
						<td>{{ $product->price }}</td>
						<td>{{ $product->available_quantity }}</td>
						<td>{{ empty($product->in_out_stock)?'In Stock':'Out of Stock' }}</td>
						<td>{{ $product->short_intro }}</td>
						<td>
							<a href="/products/edit/{{ $product->id }}" class="btn btn-info btn-sm">Edit</a><br>
							<a href="/products/delete/{{ $product->id }}" onclick="deleteAction(event)" class="btn btn-danger btn-sm">Delete</a>
						</td>
					</tr>
					@endforeach
				</tbody>



			</table>
		</div>
	</div>
</div>
@endsection

<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
