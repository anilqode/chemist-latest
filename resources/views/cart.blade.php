@extends('layout')

{{-- @section('title', 'Shopping Cart') --}}

@section('extra-css')
    <link rel="stylesheet" href="{{ asset('css/algolia.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/vuetify/dist/vuetify.min.css">
@endsection

@section('content')

   {{--  @component('components.breadcrumbs')
        <a href="#">Home</a>
        <i class="fa fa-chevron-right breadcrumb-separator"></i>
        <span>Shopping Cart</span>
    @endcomponent --}}

    <div class="cart-section container animated fadeInUp animatedfadeInUp">
        <div>
            @if (session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif

            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if (Cart::count() > 0)

            <h2>{{ Cart::count() }} item(s) in Shopping Cart</h2>

            <div class="cart-table">
                @foreach (Cart::content() as $item)
                <div class="cart-table-row">
                    <div class="cart-table-row-left">
                        <a href="{{ route('shop.show', $item->model->id) }}"><img src="{{ productImage($item->model->featured_image1) }}" alt="item" class="cart-table-img"></a>
                        <div class="cart-item-details">
                            <div class="cart-table-item"><a href="/product-detail/{{ $item->model->id }}">{{ $item->model->name }}</a></div>
                            <div class="cart-table-description">{{ $item->model->details }}</div>
                        </div>
                    </div>
                    <div class="cart-table-row-right">
                        <div class="cart-table-actions">
                            <form action="{{ route('cart.destroy', $item->rowId) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button type="submit" class="cart-options">Remove</button>
                            </form>

                            <!-- <form action="{{ route('cart.switchToSaveForLater', $item->rowId) }}" method="POST">
                                {{ csrf_field() }}

                                <button type="submit" class="cart-options">Save for Later</button>
                            </form> -->
                        </div>
                        <div>
                            £ {{ $item->price }}
                        </div>
                        <div>
                            <cart-update :id="`product-{{ $item->id }}`" name="`product-{{ $item->id }}`" :item='{{ json_encode($item) }}' :available-quantity="{{ json_encode($item->model->available_quantity) }}"></cart-update>
                        </div>
                        <div>{{ presentPrice($item->subtotal) }}</div>
                    </div>
                </div> <!-- end cart-table-row -->
                @endforeach

            </div> <!-- end cart-table -->

            <div class="cart-table-sm">
                <cart-list :hide-details="true"></cart-list>
            </div>
            <div class="cart-totals">
                <div class="width-40 width-sm-100">
                    <div class="row">
                        <div class="col-6 col-sm-8">Subtotal</div>
                        <div class="col">{{ presentPrice(Cart::subtotal()) }}</div>
                    </div>
                    @if((float)Cart::subtotal() != getNumbers()['newSubtotal'])
                    <div class="row">
                        <div class="col-6 col-sm-8">Total after Discount</div>
                        <div class="col">{{ presentPrice(getNumbers()['newSubtotal']) }}</div>
                    </div>
                    @endif
                    <!-- <div class="row">
                        <div class="col-6 col-sm-8">VAT ({{config('cart.tax')}}%)</div>
                        <div class="col">{{ presentPrice($newTax) }}</div>
                    </div> -->
                    <div class="row align-items-center">
                        <div class="col-md-8 col-8"><delivery-options :options="{{ json_encode($deliveries) }}"
                                :selected="{{ json_encode($deliverySelected) }}"></delivery-options></div>
                        <div class="col-4">{{ !empty($deliverySelected['price'])?'£'.$deliverySelected['price']:'' }}</div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-8 col-sm-12">
                            <apply-coupon :selected-coupon="`{{ $coupon }}`"></delivery-options>
                        </div>
                        <div class="col"></div>
                    </div>
                    <div class="row margin-top-15 text-bold font-16">
                        <div class="col-6 col-sm-8">Total</div>
                        <div class="col">{{ presentPrice($newTotal) }}</div>
                    </div>
                </div>
            </div>

            <div class="cart-buttons">
                <a href="/product-cat-list" class="button">Continue Shopping</a>
                <proceed-checkout-button :delivery='{{ json_encode($deliverySelected) }}'></proceed-checkout-button>
            </div>

            @else

                <h3>No items in Cart!</h3>
                <div class="spacer"></div>
                <a href="/product-cat-list" class="button">Continue Shopping</a>
                <div class="spacer"></div>

            @endif

        </div>

    </div> <!-- end cart-section -->

    {{-- @include('partials.might-like') --}}


@endsection

@section('extra-js')
    <script src="{{ asset('js/cart.js') }}"></script>


    <!-- Include AlgoliaSearch JS Client and autocomplete.js library -->
    <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
    <script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
    <script src="{{ asset('js/algolia.js') }}"></script>
@endsection
