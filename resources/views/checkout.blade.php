@extends('layout')

@section('extra-css')
<style>
.mt-32 {
    margin-top: 32px;
}
</style>

<script src="https://js.stripe.com/v3/"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')

<div class="container">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (session()->has('success_message'))
    <div class="spacer"></div>
    <div class="alert alert-success">
        {{ session()->get('success_message') }}
    </div>
    @endif

    @if(count($errors) > 0)
    <div class="spacer"></div>
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <h1 class="checkout-heading stylish-heading">Checkout</h1>
            <div class="checkout-section">
                <div>
                    
                        <checkout :user=" {{ json_encode($user) }} ">
                            <template slot="user-info">
                            </template>
                            <template slot="billing-details">
                            <form action="/paypal-checkout" name="paypalForm" method="POST">
                                {{ csrf_field() }}
                                <h2>Billing Details</h2>

                                <div class="form-group">
                                    <label for="email">Email Address</label>
                                    @if (auth()->user())
                                    <input type="email" class="form-control" id="email" name="email"
                                        value="{{ auth()->user()->email }}" readonly>
                                    @else
                                    <input type="email" class="form-control" id="email" name="email"
                                        value="{{ old('email') }}" required>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name"
                                        value="{{ old('name') }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <input type="text" class="form-control" id="address" name="address"
                                        value="{{ old('address') }}" required>
                                </div>

                                <div class="half-form">
                                    <div class="form-group">
                                        <label for="city">Town/City</label>
                                        <input type="text" class="form-control" id="city" name="city"
                                            value="{{ old('city') }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="province">County</label>
                                        <input type="text" class="form-control" id="province" name="province"
                                            value="{{ old('province') }}" required>
                                    </div>
                                </div> <!-- end half-form -->

                                <div class="half-form">
                                    <div class="form-group">
                                        <label for="postalcode">Postal Code</label>
                                        <input type="text" class="form-control" id="postalcode" name="postalcode"
                                            value="{{ old('postalcode') }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Phone (Optional)</label>
                                        <input type="text" class="form-control" id="phone" name="phone"
                                            value="{{ old('phone') }}">
                                    </div>
                                </div> <!-- end half-form -->

                                <div class="spacer"></div>

                                <delivery-details></delivery-details>
                                <input type="hidden" name="amount" value="{{$newTotal}}">
                                <button type="submit" id="complete-order" class="button-primary full-width">Pay with
                                    Paypal</button>
                            </form>

                            </template>
                            <template slot="stripePayment">
                            <form action="/stripe-checkout" name="stripeForm" method="POST">
                                {{ csrf_field() }}
                                <h2>Billing Details</h2>
                                <div class="form-group">
                                    <label for="email">Email Address</label>
                                    @if (auth()->user())
                                    <input type="email" class="form-control" id="semail" name="email"
                                        value="{{ auth()->user()->email }}" readonly>
                                    @else
                                    <input type="email" class="form-control" id="semail" name="email"
                                        value="{{ old('email') }}" required>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="sname">Name</label>
                                    <input type="text" class="form-control" id="sname" name="name"
                                        value="{{ old('name') }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="saddress">Address</label>
                                    <input type="text" class="form-control" id="saddress" name="address"
                                        value="{{ old('address') }}" required>
                                </div>

                                <div class="half-form">
                                    <div class="form-group">
                                        <label for="scity">Town/City</label>
                                        <input type="text" class="form-control" id="scity" name="city"
                                            value="{{ old('city') }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="sprovince">County</label>
                                        <input type="text" class="form-control" id="sprovince" name="province"
                                            value="{{ old('province') }}" required>
                                    </div>
                                </div> <!-- end half-form -->

                                <div class="half-form">
                                    <div class="form-group">
                                        <label for="spostalcode">Postal Code</label>
                                        <input type="text" class="form-control" id="spostalcode" name="postalcode"
                                            value="{{ old('postalcode') }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="sphone">Phone (Optional)</label>
                                        <input type="text" class="form-control" id="sphone" name="phone"
                                            value="{{ old('phone') }}">
                                    </div>
                                </div> <!-- end half-form -->

                                <div class="spacer"></div>

                                <stripe-delivery-details></stripe-delivery-details>
                                <input type="hidden" name="amount" value="{{$newTotal}}">
                                <button type="submit" id="scomplete-order" class="button-primary full-width">Pay with
                                    Stripe</button>
                              </form>
                            </template>
                        </checkout>

                        <div class="form-group" style="display:none;">
                            <label for="name_on_card">Name on Card</label>
                            <input type="text" class="form-control" id="name_on_card" name="name_on_card" value="">
                        </div>

                        <div class="form-group" style="display:none;">
                            <label for="card-element">
                                Credit or debit card
                            </label>
                            <div id="card-element">
                                <!-- a Stripe Element will be inserted here. -->
                            </div>

                            <!-- Used to display form errors -->
                            <div id="card-errors" role="alert"></div>
                        </div>



                    <div style="display:none;">
                        @if ($paypalToken)
                        <div class="mt-32">or</div>
                        <div class="mt-32">
                            <h2>Pay with PayPal</h2>

                            <form method="post" id="paypal-payment-form" action="/paypal-checkout">
                                @csrf

                                <input type="hidden" name="amount" value="{{ presentPrice($newTotal) }}">
                                <section>
                                    <div class="bt-drop-in-wrapper">
                                        <div id="bt-dropin"></div>
                                    </div>
                                </section>

                                <input id="nonce" name="payment_method_nonce" type="hidden" />
                                <button class="button-primary" type="submit"><span>Pay with PayPal</span></button>
                            </form>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="checkout-table-container">
                    <h2>Your Order</h2>
                    <div class="cart-table">
                        @foreach (Cart::content() as $item)
                        <div class="checkout-table-row">
                            <div class="checkout-table-row-left">
                                <img src="{{ productImage($item->model->featured_image1) }}" alt="item"
                                    class="checkout-table-img">
                                <div class="checkout-item-details">
                                    <div class="checkout-table-item">{{ $item->model->name }}</div>
                                    <div class="checkout-table-description">{{ $item->model->details }}</div>
                                    <div class="checkout-table-price">{{ $item->model->presentPrice() }}</div>
                                </div>
                                <div class="cart-table-actions">
                                    <form action="{{ route('cart.destroy', $item->rowId) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="cart-options">Remove</button>
                                    </form>
                                </div>
                                <cart-update :id="`product-{{ $item->id }}`" name="`product-{{ $item->id }}`"
                                    :item='{{ json_encode($item) }}'
                                    :available-quantity="{{ json_encode($item->model->available_quantity) }}">
                                </cart-update>
                            </div> <!-- end checkout-table -->
                            <div class="checkout-table-row-right">
                                <div>
                                    {{ presentPrice($item->subtotal) }}
                                </div>
                            </div>
                        </div> <!-- end checkout-table-row -->
                        @endforeach
                    </div> <!-- end checkout-table -->
                    <div class="cart-table-sm">
                        <cart-list :hide-details="true"></cart-list>
                    </div>
                    <div class="checkout-totals">
                        <div class="width-100">
                            <div class="row">
                                <div class="col-md-6 col-6">Subtotal</div>
                                <div class="col-md-6  col-6 text-right">{{ presentPrice(Cart::subtotal()) }}</div>
                            </div>
                            @if((float)Cart::subtotal() != getNumbers()['newSubtotal'])
                            <div class="row">
                                <div class="col-6 col-md-6">Total after Discount</div>
                                <div class="col-md-6 col-6 text-right">{{ presentPrice(getNumbers()['newSubtotal']) }}
                                </div>
                            </div>
                            @endif
                            <!-- <div class="row">
                                    <div class="col-md-6 col-6">VAT ({{config('cart.tax')}}%)</div>
                                    <div class="col-md-6 col-6 text-right">{{ presentPrice($newTax) }}</div>
                                  </div> -->
                            <div class="row d-flex align-items-center">
                                <div class="col-md-8 col-8">
                                    <delivery-options :options="{{ json_encode($deliveries) }}"
                                        :selected="{{ json_encode($deliverySelected) }}"></delivery-options>
                                </div>
                                <div class="col-md-4 col-4 text-right">
                                    {{ !empty($deliverySelected['price'])?'£'.$deliverySelected['price']:'Free' }}</div>
                            </div>
                            <div class="row align-items-center">
                                <div class="col-md-8 col-sm-12">
                                    <apply-coupon :selected-coupon="`{{ $coupon }}`"></apply-coupon>
                                </div>
                                <div class="col"></div>
                            </div>
                            <div class="row checkout-totals-total">
                                <div class="col-md-6 col-6">Total</div>
                                <div class="col-md-6 col-6 text-right">{{ presentPrice($newTotal) }}</div>
                            </div>
                        </div>
                    </div>




                    <!-- end checkout-totals -->
                </div>
            </div> <!-- end checkout-section -->
        </div>
    </div>

</div>

</div>


@endsection

@section('stripecontent')
<!-- <div style="position:relative;">
    <form action="/charge" data-confirm="Are you sure you want to submit the form?" method="POST"
        enctype="multipart/form-data">
        <input type="hidden" value="{{$newTotal}}" name="stripeTotal">

        @csrf
        <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
            data-key='pk_test_NPaMi1y8zt76vvT8v3XKw8sS00QDewqDM7' data-amount="{{$newTotal*100}}"
            data-name="Stripe Payment" data-description="Online payment through stripe"
            data-image="https://stripe.com/img/documentation/checkout/marketplace.png" data-locale="auto"
            data-currency="GBP">
        </script>
    </form>
</div> -->
@endsection





@section('extra-js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>


<script>
$(document).ready(function() {
    $('.stripe-button-el').addClass('d-none');
})

$("form button[type=submit]").click(function(event) {
    if ($(this).attr('class') == "stripe-button-el") {
        var x = document.forms["paypalForm"]["email"].value;
        if (x == "") {


            event.preventDefault();
            window.location.replace("/checkout");
            alert("Make Sure to enter all the details");
            event.stopImmediatePropagation();
            event.stopPropagation();


        } else {
            return true;
        }
    }
});
</script>

@endsection