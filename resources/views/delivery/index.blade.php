@extends('layouts.admin')

@section('content')
	<div class="container slider-index">
	<div class="card">
		<div class="row">
        	<div class="col-sm-12 admin_playzone">
        		<delivery :deliveries="{{ json_encode($deliveries) }}"></delivery>
        	</div>
		</div>
		</div>
	</div>
@endsection