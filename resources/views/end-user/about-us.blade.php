@extends('layout')

@section('content')
<section class="homepage-slider">
	<div class="container animated fadeInUp animatedfadeInUp">
		
<div class="row">

<div class="col-sm-12"><p class="text-center welcome-asia">About us</p></div>
<div class="col-sm-4">
	<img src="images/innerpages/shutterstock_713747434.jpg" width="100%">

</div>
<div class="col-sm-8">

<p class="text-left">

Nutrition Planet is a health and wellness retailers , supplying its customers with a wide range
of vitamins, minerals, health supplements, specialist foods and natural beauty products.
</p>
<p>
We are committed to make a healthy difference in people&#39;s lives every day and travel with
each customer on their wellness journey. It matters to us that our customers know we are a
reliable resource for their favourite products and also a place where highly trained health
enthusiasts are always excited to advise, explain, and share healthy inspiration. From our
huge range of top-quality supplements at competitive prices , we make it easier for our
customers to make healthier choices every day.
</p>

</div>

</div>

	</div>
</section>

<div class="container">
	
</div>
@endsection