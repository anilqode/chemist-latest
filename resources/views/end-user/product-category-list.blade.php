@extends('layout')

@section('content')
<section class="homepage-slider">
	<div class="container animated fadeInUp animatedfadeInUp">
		
<div class="row">
<div class="col-sm-12">
<!-- <p class="text-center welcome-asia">Chemist  at your Doorstep</p>
 --><p class="text-center welcome-para">Discover one of the best range of high quality vitamins, supplements, health foods, beauty and sports nutrition products. All designed to complement a healthy diet and lifestyle.</p>
</div>

</div>


		<div class="menu-product">
			<div class="row">
			@foreach($prod_cat as $key=> $cat)

				<div class="col-sm-4  bg-color ">
				<header>
					<a href="/products/{{$cat->id}}" >
						<div class="main-product">
						<h4 class="cat-name">
							{{$cat->name}}
						</h4>
	
					</div>
					</a>
				</header>
					
				</div>
				
				@endforeach
			</div>
		</div>
		

	

		
	</div>
</section>

<div class="container">
<!-- 	<p class="note-para">*No medicine can be sold unless it has first been approved by the U.S. Food and Drug Administration (FDA). The makers of the medicine do tests on all new medicines and send the results to the FDA</p>
 --></div>
@endsection