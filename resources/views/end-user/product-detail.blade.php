@extends('layout')

@section('content')
<section class="homepage-slider">
	<div class="container animated fadeInUp animatedfadeInUp">
		
		<div class="row">
			<div class="col-sm-12">
				<p class="text-center welcome-asia">{{$product_info->name}}</p>
				<p class="text-center welcome-para">{{$product_info->short_intro}}</p>
			</div>

		</div>


		<div class="menu-product">
			<div class="row">


				<div class="col-sm-6">
					<div class="imageSection">
						<div class="imageController">
							@if(file_exists($product_info->featured_image1))
								<div id="firstImage">
									<img src="/{{$product_info->featured_image1}}">
								</div>
							@endif
							@if(file_exists($product_info->featured_image2))
								<div id="secondImage">
									<img src="/{{$product_info->featured_image2}}">
								</div>
							@endif
							
							@if(file_exists($product_info->featured_image3))
								<div id="thirdImage">
									<img src="/{{$product_info->featured_image3}}">
								</div>
							@endif
						</div>
						<div class="imageDisplay">
							<img src="/{{$product_info->featured_image1}}" class="imgPanel" id="firstImageShow" style="display: block">
							<img src="/{{$product_info->featured_image2}}" class="imgPanel" id="secondImageShow" style="display:none">
							<img src="/{{$product_info->featured_image3}}" class="imgPanel" id="thirdImageShow" style="display:none">

						</div>
					</div>

				</div>
				<div class="col-sm-6 product-desc">
					<form action="{{ route('cart.store', $product_info) }}" method="POST">
						<p>Potency:	{{ $product_info->potency }}</p>
						<p>Size: {{ $product_info->size }}  <a href="#reviewForm" style="float: right;" onclick="displayReview(event)">Write a review</a>
						</p>
						<hr>
						<div class="right-align">
							<p>SKU: @if(isset($product_info->sku))
								{{ $product_info->sku }}
								@else 
								{{"Null"}} 
								@endif</p>
							<span class="price_pro">
								@if(empty($product_info->discount))
									£ {{ $product_info->price }}
								@else
									£ {{ $product_info->price-$product_info->discount }} <strike class="price-strike">£ {{ $product_info->price }}</strike>
								@endif
							</span>
							@if($product_info->in_out_stock==0 && $product_info->available_quantity >= 1)
								<p>
									<span class="in_stock"><i class="fa fa-check"></i> In Stock</span>
								</p>
							@else
								<p>
									<span class="in_stock"><i class="fa fa-close"></i> Out of Stock</span>
								</p>
							@endif
						</div>

							<hr>
							@if($product_info->in_out_stock==0 && $product_info->available_quantity >= 1)
							<div class="form-group">
								<label for="quantity">Quantity</label>
								<input type="number" class="form-control" name="quantity" value="1" min="1" max="{{ $product_info->available_quantity }}"/>
							</div>
							<button class="btn btn-info add_to_cart " type="submit">Add to Basket</button>
							@endif
							{{ csrf_field() }}
							<!-- <p class="favorite"><i class="fa fa-heart"></i><a href="#">Add to favorite</a></p> -->
						</form>
					</div>
					
				</div>
			</div>


	<!-- <div class="row descriptive-info">
		<div class="col-sm-6">
			<div class="description_details_product">
				<h4>Description:</h4>
				<p>{{ $product_info->description }}</p>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="description_details_product">
				<h4>Directions:</h4>
				<p>@if(isset($product_info->directions))
					{{ $product_info->directions }}
					@else
					{{ "Null" }}
					@endif
				</p>
				<h4>Ingredients:</h4>
				<p>@if(isset($product_info->ingredients))
					{{ $product_info->ingredients }}
					@else
					{{ "Null" }}
					@endif
				</p>
				<h4>Advisory Information:</h4>
				<p>@if(isset($product_info->advisory_information))
					{{ $product_info->advisory_information }}
					@else
					{{ "Null" }}
					@endif
				</p>

			</div>
		</div>
	</div> -->


<!--Reviews -->

	<div class="row review_form">
		<div class="tab">
			<button class="tablinks active" onclick="openTab(event, 'description');">Product Description</button>
			<button class="tablinks " onclick="openTab(event, 'useage');">Usage / Instruction</button>
			<button class="tablinks " onclick="openTab(event, 'warnings');">Warnings</button>
			<button class="tablinks " onclick="openTab(event, 'ingredients');">Ingredients</button>
			<button class="tablinks " onclick="openTab(event, 'review');">Reviews &amp; Ratings</button>
		</div>
		<div class="tabcontent" id="description" style="display: block;">
			<p><strong>Product Description</strong><br>
				{!! $product_info->description !!}
			</p>
		</div>
		<div class="tabcontent" id="useage" style="display: none;">
			@if(isset($product_info->directions))
				<p><strong>Usage:</strong><br>
				 {!! $product_info->directions !!}</p>
			@else
			<p><strong>Usage:</strong><br> {{$product_info->directions}}</p>
			<p><strong>Directions for adults and children 12 years and over</strong></p>
			<ul>
				<li>Lorem ipsum donor si tenet</li>
				<li>Lorem ipsum donor si tenet</li>
				<li>Lorem ipsum donor si tenet</li>
			</ul>
			<p><strong>Using technique</strong></p>
			<ul>
				<li>Lorem ipsum donor si tenet</li>
				<li>Lorem ipsum donor si tenet</li>
				<li>Lorem ipsum donor si tenet</li>
			</ul>
			@endif
		</div>
		<div class="tabcontent" id="warnings" style="display: none;">
			@if(isset($product_info->warnings))
				<p><strong>Warnings:</strong><br>
					{!! $product_info->warnings !!}
				</p>
			@else
			<p><strong>Warnings:</strong><br>
			 Do not take more medicine than the label tells you to.<br>
				If you are pregnant, or you need any other advice before using this product, talk to your doctor, pharmacist or nurse.<br>
				Do not use if you are allergic to any of the ingredients listed in the enclosed leaflet.<br>
			Keep out of the sight and reach of children.</p>
			@endif
		</div>
		<div class="tabcontent" id="ingredients" style="display: none;">
			<p>
				@if($product_info->ingredients)
				<strong>Ingredients:</strong><br>
				{!! $product_info->ingredients !!}
				@else
				<strong>Ingredients:</strong><br>
				{{ "Includes: Chewing Gum Base, Xylitol, Acesulfame Potassium, Polacrilin, Sodium Carbonate, Sodium Bicarbonate, Peppermint Oil, Menthol, Magnesium Oxide, E171, E321, Talc, Acacia and Carnauba Wax" }}
				@endif
			</p>
		</div>
		<div class="tabcontent" id="review" style="display: none;">
			<div id="reviewCarousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					@foreach($reviews as $review)
					@if($product_info->id === $review->product_id)
						<li data-target="#reviewCarousel" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
					@endif
					@endforeach
				</ol>
				<div class="carousel-inner">
					<?php $counter = 0; ?>
					@foreach($reviews as $review)
					@if($product_info->id == $review->product_id)
					<div class="customer-rating carousel-item {{ $loop->first ? ' active' : '' }}">
						<p><strong>{{ $review->name }}</strong></p>
						<?php
						$count = $review->overall;
						for($i = 0; $i < $count; $i++){ ?>
							<i class="fa fa-star rating selected" aria-hidden="true"></i>
						<?php } ?>
						<p>{{ $review->reviewBox }}</p>
						<?php ++$counter; ?>
					</div>
					@endif
					@endforeach
				</div>
				@if($counter > 0)
					<a class="carousel-control-prev" href="#reviewCarousel" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#reviewCarousel" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				@else
					<p class="text-center">No Reviews for this product! <a href="#reviewForm" onclick="addReview();">Click Here</a> to add one!</p>

				@endif

				
			</div>

			<p class="write_review" onclick="addReview();">
				Write a Review
			</p>
			<form method="POST" action="/product-detail/{{ $product_info->id }}" enctype="multipart/form-data" id="reviewForm" style="display: none;">
				@csrf
				<h4>Add Your Review</h4>
				<div class="form-group">
					<label for="value_money">Value for Money</label>
					<div id="rating-stars">
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('rating-stars',1);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('rating-stars',2);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('rating-stars',3);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('rating-stars',4);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('rating-stars',5);"></i>
						<input type="text" readonly name="value_money" id="value_money" value="0" class="form-control"/>
					</div>
				</div>
				<div class="form-group">
					<label for="quality">Quality</label>
					<div id="quality-stars">
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('quality-stars',1);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('quality-stars',2);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('quality-stars',3);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('quality-stars',4);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('quality-stars',5);"></i>
						<input type="text" readonly name="quality" id="quality" value="0"  class="form-control"/>
					</div>
				</div>
				<div class="form-group">
					<label for="performance">Performance</label>
					<div id="perfor-stars">
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('perfor-stars',1);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('perfor-stars',2);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('perfor-stars',3);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('perfor-stars',4);"></i>
						<i class="fa fa-star rating " aria-hidden="true" onclick="turnSelected('perfor-stars',5);"></i>
						<input type="text" readonly name="performance" id="performance" value="0"  class="form-control"/>
					</div>
				</div>
				<div class="form-group show_overallRating">
					<label for="overall">Overall Rating</label>
					<input type="hidden" class="form-control" readonly name="overall" id="overallRating" value="0"/>
					<span id="overall_rating">0/5</span>
				</div>
				<h4><strong>Share Your Experience (optional)</strong></h4>
				
				<div class="row other_details">
					<div class="col-sm-6 form-group">
						<label for="name">Display Name</label><br>
						<input type="text" class="form-control" name="name" required/>
					</div>
					<div class="col-sm-6 form-group">
						<label for="email">Email</label><br>
						<input type="text" class="form-control" name="email" required/>
					</div>
				</div>
				
				<div class="form-group">
					<label for="recommend">Would you recommend this product to others?</label><br>
					<input type="radio" name="recommend" value="Yes"/>Yes&nbsp;&nbsp;&nbsp;
					<input type="radio" name="recommend" value="No"/>No
				</div>
				<div class="form-group">
					<label for="reviewBox">Type your product review in the space provided</label>
					<textarea name="reviewBox" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<input type="hidden" name="product_id" value="{{ $product_info->id }}">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-md submit_btn">Submit Review</button>
				</div>
			</form>
		</div>
	</div>




</div>
</section>

<div class="container">
	<!-- <p class="note-para">*The Best medicines that you can find in your hometowm. </p> -->
</div>
@endsection
@section('extra-js')
<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip(); 

		$('#firstImage , #secondImage, #thirdImage').on('click', function(event){
							$showId = '#' + this.id + 'Show';
							$($showId).css('display', 'block');
							$($showId).siblings().css('display', 'none');
						});
	});
</script>
<script type="text/javascript">
	
	function turnSelected(id, value){
		var i;
		var stars;
		var rateOne, rateTwo, rateThree, overallRate;
		stars = document.getElementById(id).children;
		for(i = 0; i < 5; i++){
			if(stars[i].classList.contains("selected"))
			{
				stars[i].classList.remove("selected");
			}
		}
		for (i = 0; i < value; i++){	
			stars[i].classList.add("selected");
		}
		stars[5].value = value ;
		rateOne = parseFloat(document.getElementById('value_money').value);
		rateTwo = parseFloat(document.getElementById('quality').value);
		rateThree = parseFloat(document.getElementById('performance').value);
		overallRate = (rateOne + rateTwo + rateThree)/3;
		overallRate = Math.round(overallRate * 10)/ 10;
		overallRate = overallRate;
		document.getElementById('overall_rating').innerHTML = overallRate + '/5';
		document.getElementById('overallRating').value = overallRate;
	}


	function openTab(evt, tabName){
		var tabcontent = document.getElementsByClassName('tabcontent');
		var tablinks = document.getElementsByClassName('tablinks');
		var i;
		for(i = 0; i < tablinks.length; i++){
			tablinks[i].classList.remove("active");
		}
		for(i = 0; i < tabcontent.length; i++){
			tabcontent[i].style.display = "none";
		}
		evt.currentTarget.classList.add("active");
		document.getElementById(tabName).style.display = "block";
	}
	function addReview(){
		document.getElementById("reviewForm").style.display = "block";
	}

	function displayReview(e){
		var makeReview = document.getElementById("reviewForm");
		var review = document.getElementById("review");
		var tab = review.className;
		openTab(e, 'review');
		tab.className += "active";
		addReview();
		window.location.href = "#reviewForm";
	}
</script>
@endsection