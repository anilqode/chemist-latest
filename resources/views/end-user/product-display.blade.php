@section('title', 'Create Product Category')
@extends('layouts.admin')
@push('mystyles')
	<link rel="stylesheet" type="text/css" href="{{ url('css/app.css') }}">



	<style type="text/css">
		.roles-list-title{
			top: -6px !important;
			margin-bottom: 8px;
			margin-left:12px;
		}
		.inventrary_list {
			/*box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);*/
			border-radius: 10px;
			background-color: #ffffff !important;
			min-height: 556px;
		}
		.ptpl{
			padding-top: 15px;
			padding-left: 29px;
		}
		.create-form-group select{
			width: 100%;
			margin: 14px 0 0 0;
		}
		.card_img_box {
			padding: 40px 20px 36px !important;
		}
		.dealer_setting_img img, .dealer_logo_url {
			max-width: 70px !important;
			/* width: 100%; */
		}
		.card_img_box h6 {
			margin-top: 5px;
			color: #999999;
			font-weight: 400 !important;
			font-size: 18px;
			padding-top: 34px !important;
		}
		select {
			width: 100%;
			height: 50px !important;
			border-radius: 10px !important;
			border: 1px solid #cccccc;
			background: url(/img/down_arrow.png) 98.5% no-repeat;
			background-color: #ffffff;
			-webkit-appearance: none;
			float: right;
			font-size: 18px !important;
			color: #999 !important;
		}

		.product_count.center, .product_cat_count, .user_count {
			border: 1px solid #000;
			text-align: center;
			padding-top: 23px;
			font-size: 20px;
			margin-top: 99px;
			-webkit-box-shadow: 1px 1px 1px #000;
			box-shadow: 1px 1px 1px #000;
		}
	</style>
@endpush
@section('content')

	<div class="container-fluid container-wrapper mt-19">
		<section class="p_15 mb-63 dashboard_inventary  animated fadeInUp animatedfadeInUp">
			<div class="row">
				
				<div class="col-sm-12 admin_playzone">
					<div class="row">
						<div class="col-sm-12">
							<table class="table table-hover product-list-display display" id="example">
								<thead>
								<th>ID</th>
								<th>Category Image</th>
								<th>Category Name</th>
								<th>Description</th>
								<th>Order</th>
								<th width="200px">Action</th>
								</thead>

								<tbody>
								@foreach($display as $catlist)
									<tr>
										<td>{{ $catlist->id }}</td>
										<td><img src="/{{ $catlist->featured_image }}" alt="{{ $catlist->name }}" width="200px"/></td>
										<td>{{ $catlist->name }}</td>
										<td>{{ $catlist->description }}</td>
										<td>{{ $catlist->order }}</td>
										<td  width="200px">
											<a href="/product-category/edit/{{ $catlist->id }}" class="btn btn-info">Edit</a><br>
											<a href="/product-category/delete/{{ $catlist->id }}" onclick="deleteAction(event)" class="btn btn-danger">Delete</a>
										</td>
									</tr>
								@endforeach
								</tbody>

							</table>
						</div>
					</div>
				</div>

			</div><!-- .row -->
		</section>
	</div><!-- .container-fluid -->
@endsection
@push('myScripts')
	<script type="text/javascript" src="{{ url('/js/jquery.min.js') }}"></script>

@endpush
