@extends('layouts.admin')
<?php
use App\User;
use App\ProductCategory;
use App\Product;
?>

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-sm-3">
          @include('layouts.sidebar')
        </div>
        <div class="col-sm-9">
            <div class="admin_playzone">

                @role('super-admin')
                <div class="row admin-db">
                <div class="col-sm-4">
                    <div class="product_count center">
                        <i class="fa fa-shopping-basket" aria-hidden="true"></i>

                        <a href="/products-show"><p>Products</p></a>
                         <a href="/products-show"> <p><span>{{ count(Product::all()) }}</span></p></a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="product_cat_count center">
                        <i class="fa fa-tags" aria-hidden="true"></i>
                        <a href="product-category"><p>Products Categories</p></a>
                       <a href="product-category"> <p><span>{{ count(ProductCategory::all()) }}</span></p></a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="user_count center">
                        <i class="fa fa-user" aria-hidden="true"></i>
                     <a href="users"><p>Users</p></a>   
                        <a href="users"> <p><span>{{ count(User::all()) }}</span></p></a>
                    </div>
                </div>
            </div>
                @else
                    <div class="row justify-content-md-center">
                
                    <div class="col-sm-4">
                       <i class="fa fa-user-circle" aria-hidden="true"></i>
                        <div class="user_dash product_count center">
                           
                            <a href="/profile">User Information</a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <i class="fa fa-first-order" aria-hidden="true"></i>
                        <div class="user_dash product_count center">
                            <a href="/order-history">Orders</a>
                        </div>
                    </div>
                </div>
                
                @endrole

            </div>
        </div>
    </div>
</div>
@endsection

