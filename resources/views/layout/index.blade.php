@extends('layout')


@section('content')

<section class="homepage-  animated fadeInUp animatedfadeInUp">

	<div id="homepageCarousel" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			@foreach($slider as $slide)
			<li data-target="#homepageCarousel" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
			@endforeach
		</ol>
		<div class="carousel-inner">
			@foreach($slider as $slide)
			<div class="carousel-item {{ $loop->first ? ' active' : '' }}">
				<img src="{{ $slide->image_url }}" alt="{{ $slide->alt}}" title="{{ $slide->title }}">
			</div>
			@endforeach
		</div>

		<a class="carousel-control-prev" href="#homepageCarousel" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#homepageCarousel" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 animated fadeInUp animatedfadeInUp">
				<div class="col-sm-12 text-center"><a href="/product-cat-list" class="btn link-btn margin-top-30">SHOP BY CATEGORY</a></div>

			</div>

		</div>
		<div class="container">

			<div class="menu-product animated fadeInUp animatedfadeInUp">
				<div class="row no-gutters">
					@foreach($main_menu as $key=> $cat)

					<div class="col-sm-4 product-list product-listing{{$key+1}}" style="background:url(<?php echo $cat->featured_image;?>);background-repeat:no-repeat;background-size:cover;">
						<header>
							<img src="images/homepage-images/plus-dotted.png"/ alt='medicine'>
						</header>
						<div class="main-product">
							<h4>
								{{$cat->menuName}}
							</h4><br>
							<a href="/main-menu/{{$cat->id}}" class="btn link-btn">View {{$cat->menuName}}</a>
						</div>
					</div>
					@if($key==5)
					<?php break; ?>
					@endif
					@endforeach
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 text-center"><a href="/product-cat-list" class="btn link-btn margin-top-90">View All Our Categories</a></div>
		</div>


			<!-- <div class="vedio-ambassador animated fadeInUp animatedfadeInUp"> -->
				<div class="row quality-part">
					<div class="col-sm-12 quality-assurance">
						
						<div class="col-sm-12 hidden-part">
							<div class="col-sm-7">
								<div class="right-art">
									<h2>Mission</h2>
									<ul>
										<li><h4>Committed to providing the highest quality</h4></li>
										<li><h4>Helping customers eat healthily and live better</h4></li>
										<li><h4>Looking after our colleagues in a culture of trust and respect</h4></li>
									</ul>
									
								</div>
							</div>
							
						</div>
					</div>

				</div>

			<!-- </div>
		 -->
	</div>
</section>


@endsection