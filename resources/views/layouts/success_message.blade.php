@if(Session::has('success'))
<div class="container">
	<div class="row">
		<div class="col-sm-12">
		    <div class="alert alert-success">
		        <strong>Congratulation! </strong>{{Session::get('success')}}
		    </div>
    	</div>
	</div>
</div>
@endif