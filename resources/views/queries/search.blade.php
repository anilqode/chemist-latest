@extends('layout')

@section('content')
<div class="container search-results">
	@if(isset($queries))
	<div class="results-heading">
		<h3> Search Results for <b> {{ $query }} </b> :</h3>
		<em>Shows results from approximately {{ count($queries) }} hints</em>
	</div>
	<div class="row result-products">
		@foreach($queries as $result)		
		<div class="col-sm-3">
			<div class="product-image text-center">
				<a href="{{ url('/product-detail', [$result->id]) }}"><img class="search-product-image" src="/{{ $result->featured_image1 }}" alt="{{$result->name}}"/></a>
			</div>
			<div class="product-description">
				<a href="{{ url('/product-detail', [$result->id]) }}"><h3>{{ $result->name }}</h3></a>
				<p class="text-center">Price:  ‎£ {{ $result->price }}</p>
			</div>
		</div>			
		@endforeach
	</div>
	@endif
</div>
@endsection
