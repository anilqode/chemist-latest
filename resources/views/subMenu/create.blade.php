@extends('layouts.admin')

@section('content')
<div class="container-fluid">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div class="row">
		<div class="col-sm-3 admin_sidebar">
			
		</div>
		<div class="col-sm-9 col-sm-8">
			<form method="post" action="{{url('/subMenu/create')}}">
			@csrf
				<h3>Add Sub Menu</h3>
				<div class="form-group">
					<input type="hidden" value="{{csrf_token()}}" name="_subMenuName"/>
					<label for="subMenuName">Sub Menu Name:</label>
					<input class="form-control" type="text" name="subMenuName"/>
				</div>
				<div class="form-group">
					<label for="order">Sub Menu Order:</label>
					<input class="form-control" type="number" name="order"/>
				</div>
				<div class="form-group">
					<input type="hidden" value="{{csrf_token()}}" name="_mainmenuid"/>
					<label for="mainmenuid">Select main menu:</label>
					<select class="form-control" name="mainmenuid">
							<option value="">SELECT MENU</option>
						@foreach($main_Menu as $mainmenu)
							<option value="{{ $mainmenu->id }}">{{ $mainmenu->menuName }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<button type="submit" class="form-control btn btn-primary btn-block">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection