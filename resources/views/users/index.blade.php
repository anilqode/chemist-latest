@extends('layouts.admin')

@section('content')
	<div class="container-fluid slider-index">
	<div>
		<div class="row margin-top-30">
        	<div class="col-sm-12 admin_playzone">
        	  <users-list :users="{{ json_encode($users)}}"></users-list>
        	</div>
		</div>
		</div>
	</div>
@endsection