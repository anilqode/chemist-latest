<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. Thes
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', 'HomeController@index')->name('home');

route::get('SendEmailToAdmin','EmailController@SendEmailToAdmin');

Route::get('/our-guarantee' , 'InnerPageController@ourGuarantee');
Route::get('/privacy-policy' , 'InnerPageController@privacyPolicy');
Route::get('/terms-of-use' , 'InnerPageController@termsOfUse');
Route::get('/terms-and-condition', 'InnerPageController@termsandCondition');
Route::get('/return-policy' , 'InnerPageController@deliveryInfo');
Route::get('/help-faq' , 'InnerPageController@deliveryInformation');
Route::get('/requestcallback' , 'RequestCallbackController@index');
Route::post('/requestcallback', 'RequestCallbackController@store');
Route::get('/review', 'ReviewController@show');
Route::get('/suggest-and-win', 'SuggestandWinController@index');
Route::post('/suggest-and-win', 'SuggestandWinController@store');
Route::get('/reward-points' , 'InnerPageController@rewardPoints');


Route::get('/', 'HomepageController@index');
Route::get('/about-us','AboutUsController@showAboutPage');
Route::get('/terms','AgreeController@showTerms');
Route::get('/privacy','AgreeController@showPrivacy');
Route::get('/contact-us','ContactUsController@contactUs');
Route::get('/gallery','ContactUsController@gallery');
Route::resource('/product-category', 'ProductCategoryController',['only' => ['show']]);
Route::get('/product-cat-list', 'ProductController@displayProdCatList');
Route::resource('/product', 'ProductController',['only' => ['show']]);
Route::get('products/{cat_id}','ProductCategoryController@showCategoryProducts');
Route::get('product-detail/{id}','ProductCategoryController@productDetail')->name('product-detail');
Route::get('/sub-menu/{id}','HomepageController@showSubmenuProducts');
Route::get('/main-menu/{id}','HomepageController@showMainmenuProducts');


// Route::get('product-detail/{id}/review', 'ReviewController@index');



Route::get('/redirect_to_register_for_auth', 'ReviewController@checkAuth')->middleware('auth');
Route::post('product-detail/{id}', 'ReviewController@checkAuthentication');


Route::get('/cart', 'CartController@index')->name('cart.index');
Route::post('/cart/{product}', 'CartController@store')->name('cart.store');
Route::patch('/cart/{product}', 'CartController@update')->name('cart.update');
Route::post('/update-delivery/', 'CartController@updateDelivery')->name('cart.update-delivery');
Route::post('/update-coupon/', 'CartController@updateCoupon')->name('cart.update-coupon');
Route::delete('/cart/{product}', 'CartController@destroy')->name('cart.destroy');
Route::post('/cart/switchToSaveForLater/{product}', 'CartController@switchToSaveForLater')->name('cart.switchToSaveForLater');
Route::delete('/saveForLater/{product}', 'SaveForLaterController@destroy')->name('saveForLater.destroy');
Route::post('/saveForLater/switchToCart/{product}', 'SaveForLaterController@switchToCart')->name('saveForLater.switchToCart');

Route::get('/shop', 'ShopController@index')->name('shop.index');
Route::get('/shop/{product}', 'ShopController@show')->name('shop.show');

Route::get('/checkout', 'CheckoutController@index')->name('checkout.index');
Route::post('/checkout', 'CheckoutController@store')->name('checkout.store');

//temporary use


	


//Route::group(['middleware' => ['auth']], function() {
Route::group(['middleware' => ['auth', 'role:super-admin']], function() {

		Route::resource('/product-category', 'ProductCategoryController');
		Route::get('/product-category/delete/{id}', 'ProductCategoryController@delete');
		Route::get('/product-category/edit/{id}', 'ProductCategoryController@edit');
		Route::post('/product-category/edit/{id}', 'ProductCategoryController@update');

		Route::get('products-show', 'ProductController@show');
		Route::get('products/delete/{id}', 'ProductController@delete');
		Route::get('products/edit/{id}', 'ProductController@edit');
		Route::post('products/edit/{id}', 'ProductController@update');

		Route::get('products/show/{id}', 'ProductController@visualize');

		Route::resource('/product', 'ProductController');
		Route::resource('/delivery', 'DeliveryController');



		Route::get('/slider', 'SliderController@index');
		Route::get('/slider/create','SliderController@create');
		Route::post('/slider/create', 'SliderController@store');
		Route::get('/slider/edit/{id}', 'SliderController@edit');
		Route::post('/slider/edit/{id}', 'SliderController@update');
		Route::get('/slider/delete/{id}', 'SliderController@destroy');


		

		Route::resource('/users', 'UserController');
	
		Route::get('/menu', 'MenuController@index');
		Route::get('/menu/create', 'MenuController@create');
		Route::post('/menu/create', 'MenuController@store');
		Route::get('/menu/edit/{id}', 'MenuController@edit');
		Route::post('/menu/edit/{id}', 'MenuController@update');
		Route::get('/menu/delete/{id}', 'MenuController@destroy');

		Route::get('/subMenu', 'SubMenuController@index');
		Route::get('/subMenu/create', 'SubMenuController@create');
		Route::post('/subMenu/create', 'SubMenuController@store');
		Route::get('/subMenu/edit/{id}', 'SubMenuController@edit');
		Route::post('/subMenu/edit/{id}', 'SubMenuController@update');
		Route::get('/subMenu/delete/{id}', 'SubMenuController@destroy');

		Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

		Route::get('/coupon', 'CouponsController@index')->name('coupon.index');
		Route::post('/coupon', 'CouponsController@store')->name('coupon.store');
		Route::put('/coupon/{id}', 'CouponsController@update')->name('coupon.update');
		Route::delete('/coupon/{id}', 'CouponsController@destroy')->name('coupon.destroy');
		Route::get('/terms-create-edit', 'AgreeController@createTerms')->name('agreement.terms');
		Route::get('/privacy-create-edit', 'AgreeController@createPrivacy')->name('agreement.privacy');
		Route::post('/save-agreement', 'AgreeController@store')->name('agreement.save');
		Route::put('/save-agreement/{id}', 'AgreeController@update')->name('agreement.update');

	
});
Route::group(['middleware' => ['auth']], function() {
	Route::get('/profile','UserController@userprofile');
	// Route::get('/changePassword','HomeController@changePassword');
	Route::get('/order-history', 'OrderController@orderhistory')->name('order-history');
	Route::put('/dispatch/{order}', 'OrderController@dispatchOrder')->name('dispatch');
	Route::get('/re-order/{id}', 'OrderController@reOrder');
	Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );
	Route::get('/exportCSV', 'OrderController@export');

	Route::get('/changePassword','UserController@showChangePasswordForm');
	Route::post('/changePassword','UserController@changePassword')->name('changePassword');
});

//paypal routes
Route::post('/paypal-checkout','PaymentController@payWithPaypal');
Route::get('/paypal-checkout-status','PaymentController@getPaymentStatus')->name('status');

//stripe routes
Route::post('/stripe-checkout', 'StripePaymentController@checkout');
Route::get('/stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/payment-confirmation', 'PaymentController@paymentConfirmation')->name('payment-confirmation');


Route::post('/queries/search', 'QueryController@search');
Route::post('/checkout-login', 'Auth\LoginController@postLogin');

Route::post('/charge', 'CheckoutStripeController@charge');